
# Spring Boot 3 with Java sample project

This is a sample project on Spring Boot 3 with java 17.

## Host

To run this project, you will need to add the following dns to hosts file

`127.0.0.1 kafka`


## Usage/Examples

Start docker stack with next command:
```bash
docker-compose up -d
```
Build application:
```bash
gradlew build -x test
java -jar build/libs/sb3_java_sample-0.0.1-SNAPSHOT.jar
```
Create new employee:
```bash
curl -X POST -H "content-type: application/json" http://localhost:8000/api/employees -d '{"name":"Jhon Doe","email":"jhondoe@mail.com","birthdate":"2000-01-01","rank":50,"gender":"MALE","salary":5000,"active":true}'
```
Sample response:
```log
2022-11-27T18:12:22.273-05:00  INFO 1452 --- [nio-8000-exec-2] i.s.s.listener.LocalListener             : Employee saved: Employee(id=f42e5cab-6245-4108-b218-ec70bbd33bda, name=Jhon Doe, email=jhondoe@mail.com, birthdate=2000-01-01, rank=50, gender=MALE, salary=5000.0, active=true)
Invoking function: streamBridge<class java.lang.Object, class java.lang.Object>with input type: class java.lang.Object
2022-11-27T18:12:22.320-05:00  INFO 1452 --- [container-0-C-1] i.s.s.listener.KafkaListener             : message of employee: GenericMessage [payload=Employee(id=f42e5cab-6245-4108-b218-ec70bbd33bda, name=Jhon Doe, email=jhondoe@mail.com, birthdate=2000-01-01, rank=50, gender=MALE, salary=5000.0, active=true), headers={deliveryAttempt=1, kafka_timestampType=CREATE_TIME, kafka_receivedMessageKey=[B@6ab6a375, kafka_receivedTopic=io.spring.sb3_java_sample.employees, kafka_offset=1, scst_nativeHeadersPresent=true, kafka_consumer=org.apache.kafka.clients.consumer.KafkaConsumer@6a3b983e, source-type=kafka, id=f4dbbff1-eb2f-a606-55da-8db62be71eea, kafka_receivedPartitionId=0, contentType=application/json, kafka_receivedTimestamp=1669590742294, kafka_groupId=loggers, timestamp=1669590742320}]
```

## License

[Apache License](https://choosealicense.com/licenses/apache-2.0/)


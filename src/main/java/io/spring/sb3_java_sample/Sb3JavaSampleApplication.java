package io.spring.sb3_java_sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb3JavaSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sb3JavaSampleApplication.class, args);
	}

}

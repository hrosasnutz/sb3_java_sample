package io.spring.sb3_java_sample.api;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

import io.spring.sb3_java_sample.data.Employee;
import io.spring.sb3_java_sample.data.EmployeeRepository;
import io.spring.sb3_java_sample.dto.EmployeeApplicationEvent;
import io.spring.sb3_java_sample.util.Validations;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    
    private EmployeeRepository repository;
    
    private ApplicationEventPublisher publisher;

    public EmployeeController(EmployeeRepository repository, ApplicationEventPublisher publisher) {
        this.repository = repository;
        this.publisher = publisher;
    }

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(repository.findAll());
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable UUID id) {
        return ResponseEntity.of(repository.findById(id));
    }
    
    @PostMapping
    @Transactional
    public ResponseEntity<?> save(@Validated(value = Validations.OnApiRequest.class) @RequestBody Employee employee, UriComponentsBuilder builder) {
        employee = repository.save(employee);
        publisher.publishEvent(new EmployeeApplicationEvent(employee));
        return ResponseEntity
                .created(builder
                    .buildAndExpand(employee.getId()).toUri()
                )
                .build();
    }
}

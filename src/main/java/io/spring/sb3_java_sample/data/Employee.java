package io.spring.sb3_java_sample.data;

import java.time.LocalDate;
import java.util.UUID;

import io.spring.sb3_java_sample.util.Validations;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "employees")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Null(message = "The id must be null.", groups = Validations.OnApiRequest.class)
    private UUID id;
    @NotBlank(message = "The name must not be blank.")
    private String name;
    @Email(message = "The email must be correct format.")
    private String email;
    @NotNull(message = "The birthdate must not be null.")
    private LocalDate birthdate;
    @NotNull(message = "The rank must not be null.")
    private Integer rank;
    @Enumerated(EnumType.STRING)
    @NotNull(message = "The gender must not be null.")
    private Gender gender;
    @NotNull(message = "The salary must not be null.")
    @Min(value = 1500, message = "The salary value must be greater equal than 1500.")
    @Max(value = 8000, message = "The salary value must be lower equal than 8000.")
    private Double salary;
    @NotNull(message = "The active must not be null.")
    private Boolean active;
}

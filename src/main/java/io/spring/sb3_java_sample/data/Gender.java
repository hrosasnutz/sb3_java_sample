package io.spring.sb3_java_sample.data;

public enum Gender {
    MALE, FEMALE
}

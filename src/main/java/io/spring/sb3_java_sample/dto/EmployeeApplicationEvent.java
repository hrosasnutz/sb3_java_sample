package io.spring.sb3_java_sample.dto;

import org.springframework.context.ApplicationEvent;

import io.spring.sb3_java_sample.data.Employee;

public class EmployeeApplicationEvent extends ApplicationEvent {

    public EmployeeApplicationEvent(Employee employee) {
        super(employee);
    }
    
    public Employee getEmployee() {
        return (Employee) getSource();
    }
}

package io.spring.sb3_java_sample.listener;

import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

import io.spring.sb3_java_sample.data.Employee;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KafkaListener {
    
    @Bean
    public Consumer<Message<Employee>> logging() {
        return msg -> log.info("message of employee: {}", msg);
    } 
}

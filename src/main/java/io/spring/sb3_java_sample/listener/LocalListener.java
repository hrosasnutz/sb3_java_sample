package io.spring.sb3_java_sample.listener;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.nio.charset.StandardCharsets;

import io.spring.sb3_java_sample.dto.EmployeeApplicationEvent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LocalListener {
    
    private StreamBridge streamBridge;

    public LocalListener(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    @TransactionalEventListener
    public void employeeEvents(EmployeeApplicationEvent event) {
      log.info("Employee saved: {}", event.getEmployee());
      var message = MessageBuilder.withPayload(event.getEmployee())
              .setHeader(KafkaHeaders.KEY, event.getEmployee().getId().toString().getBytes(StandardCharsets.UTF_8))
              .build();
        streamBridge.send("employees-out-0", message);
    }
}

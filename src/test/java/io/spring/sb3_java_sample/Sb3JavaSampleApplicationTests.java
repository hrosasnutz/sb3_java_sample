package io.spring.sb3_java_sample;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@EmbeddedKafka(
	ports = 9092,
      	topics = "io.spring.sb3_java_sample.employees",
	partitions = 1
)
public class Sb3JavaSampleApplicationTests {

	@Test
	void contextLoads() {
	}

}

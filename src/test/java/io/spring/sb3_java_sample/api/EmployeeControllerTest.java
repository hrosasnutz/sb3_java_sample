package io.spring.sb3_java_sample.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.hamcrest.core.IsAnything;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import io.spring.sb3_java_sample.Sb3JavaSampleApplicationTests;
import io.spring.sb3_java_sample.data.EmployeeRepository;
import io.spring.sb3_java_sample.util.Factories;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

class EmployeeControllerTest extends Sb3JavaSampleApplicationTests {
    
    private MockMvc mvc;
    
    @Autowired
    private EmployeeRepository repository;
    
    @Autowired
    private ObjectMapper mapper;
    
    @BeforeEach
    void setUp(WebApplicationContext wac) {
        mvc = webAppContextSetup(wac).build();
    }

    @Test
    void getAll() throws Exception {
        var employees = Factories.getListOfEmployee(10, false);
        employees = repository.saveAll(employees);
        mvc.perform(get("/api/employees"))
           .andExpectAll(
                status().isOk(),
                content().contentType(MediaType.APPLICATION_JSON)
            )
           .andExpect(jsonPath("$.length()", IsEqual.equalTo(10)));
    }

    @Test
    void getById() throws Exception {
        var employee = Factories.getEmployee(null);
        employee = repository.save(employee);
        mvc.perform(get("/api/employees/{id}", employee.getId()))
           .andExpectAll(
                   status().isOk(),
                   content().contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(jsonPath("$.id", IsEqual.equalTo(employee.getId().toString())));
    }
    
    @Test
    void save() throws Exception {
        var employee = Factories.getEmployee(null);
        mvc.perform(
                post("/api/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsBytes(employee))
            )
           .andExpect(status().isCreated())
           .andExpect(header().string(HttpHeaders.LOCATION, IsAnything.anything()));
    }
}
package io.spring.sb3_java_sample.data;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import io.spring.sb3_java_sample.Sb3JavaSampleApplicationTests;
import io.spring.sb3_java_sample.util.Factories;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest extends Sb3JavaSampleApplicationTests {

    @Autowired
    private EmployeeRepository repository;
    
    @BeforeEach
    void setUp() {
    }
    
    @Test
    public void testSaveEmployee() {
        var employee = Factories.getEmployee(null);
        employee = repository.save(employee);
        Assertions.assertThat(employee).isNotNull();
        Assertions.assertThat(employee.getId()).isNotNull();
    }
}
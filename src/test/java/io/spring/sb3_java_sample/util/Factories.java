package io.spring.sb3_java_sample.util;

import com.github.javafaker.Faker;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.IntStream;

import io.spring.sb3_java_sample.data.Employee;
import io.spring.sb3_java_sample.data.Gender;

public class Factories {
    
    private static final Faker faker = new Faker();
    
    public static Employee getEmployee(UUID uuid) {
        return new Employee(
                uuid,
                faker.name().name(),
                faker.internet().emailAddress(),
                LocalDate.ofInstant(faker.date().birthday().toInstant(), ZoneId.systemDefault()),
                faker.random().nextInt(0, 100),
                faker.random().nextInt(10) > 5 ? Gender.FEMALE : Gender.MALE,
                faker.number().randomDouble(2, 1500, 8000),
                faker.bool().bool()
        );
    }
    
    public static Collection<Employee> getListOfEmployee(int total, boolean withUuid) {
        return IntStream.range(0, total)
                .mapToObj(i -> withUuid ? getEmployee(UUID.randomUUID()): getEmployee(null))
                .toList();
    }
}
